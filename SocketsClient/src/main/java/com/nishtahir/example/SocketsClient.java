package com.nishtahir.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Nish on 3/28/14.
 */
public class SocketsClient {

    //This is the computers local host ip address.
    private static final String LOCAL_HOST = "127.0.0.1";

    public static void main(String[] args) {
        //Socket for the client
        Socket mClientSocket = null;
        //print writer for the sockets output stream
        PrintWriter mPrintWriter = null;
        //buffered reader to connect to the sockets input stream
        BufferedReader mBufferedReader = null;

        try {
            mClientSocket = new Socket(LOCAL_HOST, 10007);
            mPrintWriter = new PrintWriter(mClientSocket.getOutputStream(), true);
            mBufferedReader = new BufferedReader(new InputStreamReader(mClientSocket.getInputStream()));

        } catch (IOException e) {
            //An error here could either the host LOCAL_HOST does not exist
            //this can also be caught with an UnknownHostException
            System.out.println("Oops, something went wrong somewhere...");
            e.printStackTrace();
        }

        //This is just an alternative to using something like Scanner
        //To get input from the console
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(System.in));
        String input;
        try {
            while((input = stdInput.readLine()) != null){
                mPrintWriter.println(input);
                System.out.println("Echo: " + input);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
        //Close all the sockets and streams to prevent memory leaks
        mBufferedReader.close();
        mClientSocket.close();
        mPrintWriter.close();
        stdInput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
