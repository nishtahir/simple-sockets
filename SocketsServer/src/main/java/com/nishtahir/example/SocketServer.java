package com.nishtahir.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Nish on 3/28/14.
 */
public class SocketServer {

    //Use higher port number to avoid conflict with other
    //software
    private static final int PORT_NUMBER = 10007;

    //Exit code to indicate that the program crashed
    private static final int EXIT_FAIL = 1;


    public static void main(String[] args) {

        //Server socket to receive connections
        ServerSocket mServerSocket = null;

        try{
            //Create socket with port number
            mServerSocket = new ServerSocket(PORT_NUMBER);
            System.out.println("Listening for connections...");

        }catch (IOException e){
            //Print error message - An error here means that the socket could not be
            //created
            System.out.println("Error creating socket...");
            e.printStackTrace();
            //Terminate the program
            System.exit(EXIT_FAIL);
        }
        //Client socket to receive data
        Socket mClientSocket = null;

        try{
            //Accept the connection from the client socket to the server socket
            mClientSocket = mServerSocket.accept();
            System.out.println("Connected to client...");
        }catch (IOException e) {
            //Print error message - An error here means that the server could not connect to
            //the client
            System.out.println("Error connecting to client...");
            e.printStackTrace();
            //Terminate the program
            System.exit(EXIT_FAIL);
        }
        try {
            //Print writer allows us to print to the clients output stream
            PrintWriter mPrintWriter = new PrintWriter(mClientSocket.getOutputStream(), true);

            //Reader allows us to read content from the clients input stream
            BufferedReader mBufferedReader = new BufferedReader(new InputStreamReader(mClientSocket.getInputStream()));

            String input;
            while ((input = mBufferedReader.readLine()) != null ){
                System.out.println("Server: " + input);

                //Command from the client to stop the server
                if(input.equals("~Server stop")){
                    break;
                }
            }

            //Close all the connections and streams to prevent memory leaks
            mPrintWriter.close();
            mBufferedReader.close();
            mServerSocket.close();
            mClientSocket.close();

        } catch (IOException e) {
            //An error here means that there was a problem passing data between the
            // client and server
            e.printStackTrace();
        }
    }
}
